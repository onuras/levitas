"""
Tons of data available in http://api.bitcoincharts.com/v1/csv/
But this data requires some processing before used

I am planning to use coinbaseUSD table in this project:
http://api.bitcoincharts.com/v1/csv/coinbaseUSD.csv.gz

But there are huge gaps in front of this data until
Mon Jan 26 00:01:02 2015

This script should skip all gaps and start from this date

Bitcoincharts provides data with following values:
1422859059,223.240000000000,0.020000000000
date,price,volue
"""

import sys
import csv
from matplotlib import pyplot
from pandas import read_csv


class Converter():
    """ Converts data from bitcoincharts to x amount of interval charts

    Example:
        with Converter(interval=1440, output_file_path='out.csv') as converter:
            for line in sys.stdin:
                converter.feed(line)
    """

    # Data interval (minutes)
    interval = 5

    # Skip values until
    # Mon Jan 26 00:01:02 2015: 1422223262
    # 1492808400 use this for testing purpuses
    skip_until = 1492808400

    output_file_path = None

    _openp = 0
    _closep = 0
    _low = 0
    _high = 0
    _start_date = 0
    _total_volume = 0
    _output_file = None
    _csv_writer = None

    def __init__(self, interval=None, skip_until=None, output_file_path=None):
        if interval:
            self.interval = interval
        if skip_until:
            self.skip_until = skip_until
        if output_file_path:
            self.output_file_path = output_file_path
        if self.output_file_path:
            self._output_file = open(self.output_file_path, 'w')
            self._csv_writer = csv.writer(self._output_file)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def close(self):
        self._output_file.close()

    def feed(self, line):
        date, price, volume = map(float, line.split(","))

        if date < self.skip_until:
            return

        if self._start_date == 0:
            self._start_date = date
        if self._low == 0 and self._high == 0:
            self._openp = price
        if self._low == 0 or price < self._low:
            self._low = price
        if self._high == 0 or price > self._high:
            self._high = price

        self._total_volume += volume

        if self._start_date == 0 or \
                date - self.interval * 60 > self._start_date:
            self._closep = price

            if self._csv_writer:
                self._csv_writer.writerow([self._start_date,
                                           self._openp,
                                           self._high,
                                           self._low,
                                           self._closep,
                                           self._total_volume])

            self._low = 0
            self._high = 0
            self._start_date = date
            self._total_volume = 0


def show_data(file_path):
    """ Shows converted data. This function uses second column ()"""
    dataset = read_csv(file_path, usecols=[1], engine='python')
    pyplot.plot(dataset)
    pyplot.show()


def convert_all():
    """ Converts data from stdin to 5m, 15m, 1h, 2h and 4h interval carts """
    converter_5m = Converter(interval=5,
                             output_file_path='data/btc_price_5m.csv')

    converter_15m = Converter(interval=15,
                              output_file_path='data/btc_price_15m.csv')

    converter_1h = Converter(interval=60,
                             output_file_path='data/btc_price_1h.csv')

    converter_2h = Converter(interval=120,
                             output_file_path='data/btc_price_2h.csv')

    converter_4h = Converter(interval=240,
                             output_file_path='data/btc_price_4h.csv')

    for line in sys.stdin:
        converter_5m.feed(line)
        converter_15m.feed(line)
        converter_1h.feed(line)
        converter_2h.feed(line)
        converter_4h.feed(line)

    converter_5m.close()
    converter_15m.close()
    converter_1h.close()
    converter_2h.close()
    converter_4h.close()
