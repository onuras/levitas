# Levitas


### Resources

* [LSTM Neural Network for Time Series Prediction](https://github.com/jaungiers/LSTM-Neural-Network-for-Time-Series-Prediction)
* [Bitcoin historical data](http://api.bitcoincharts.com/v1/csv/)
* [LSTM stateful example keras](https://github.com/keras-team/keras/blob/master/examples/lstm_stateful.py)
* [Multivariate Time Series Forecasting with LSTMs in Keras](https://machinelearningmastery.com/multivariate-time-series-forecasting-lstms-keras/)
